import os
import sys
import aiohttp
from aiohttp import web
import base64
import json
from signature import *
from middlewares import setup_middlewares

host = os.getenv('SERVER_PORT', '0.0.0.0')
port = os.getenv('SERVER_HOST', '6789')
dir  = os.getenv('SERVER_DIR', './data')

if (dir[0:][:1] != '/'):
   dir = os.path.abspath(os.curdir) + '/' + dir;

os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'


async def action_test(request):
    return web.Response(text = 'We are working!!!', status=200, content_type='text/plain')

async def action_check(request):
    json_data = await request.json()
    
    signature_base64  = json_data.get('signature', '')
    signature_base64_len = len(signature_base64)

    object_url = json_data.get('object_url', '')

    data = {}

    print("Check signature for object_url: " + object_url + ", Signature length:" + str(signature_base64_len), file=sys.stderr)

    if (signature_base64):
       base64_bytes = signature_base64.encode('ascii')
       signature = base64.b64decode(base64_bytes)     
       data = analyze_signature(signature, object_url, dir)

    json_content = json.dumps(data, indent=2, ensure_ascii=False)
    print("Json response: " + json_content, file=sys.stderr)

    return web.Response(text = json_content, status=200, content_type='application/json')

print("\n\n============================================================", file=sys.stderr)
app = web.Application()

print("Registry routes", file=sys.stderr)
app.add_routes([web.get('/api/test', action_test),
                web.post('/api/check', action_check)])
setup_middlewares(app)

if __name__ == '__main__':

    print("Run web app. Working dir = "+dir, file=sys.stderr)
    web.run_app(app, host=host, port=port)
    print("Finished")
