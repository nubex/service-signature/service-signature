from signature import * 
import sys
import os

if __name__ == '__main__':

   if (len(sys.argv)>2):
      exit(2)

   object_url = ''
   if (len(sys.argv)>1):
      object_url = sys.argv[1]

   dir  = os.getenv('SERVER_DIR', './data')

   signature = sys.stdin.buffer.read()
   data = analyze_signature(signature, object_url, dir)

   for key in data:
     print(key + ' = ' + data[key])

