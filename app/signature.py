import uuid
import os
import sys
import errno
import re
import datetime as dt
import requests
from subprocess import STDOUT, check_output
import pytz
import urllib3
import base64
import binascii

def analyze_signature(signature, object_url = '', work_directory = './data'):

   print("Analyze signature", file=sys.stderr)

   try:
      tmp = re.sub(r'\r?\n?-+.+-+\r?\n', '', signature.decode("utf-8"))
      tmp = re.sub(r'\r?\n', '', tmp);
      signature = base64.b64decode(tmp.encode('utf-8'), validate=True)
   except:
      pass

   id = str(uuid.uuid4())

   filename_signature = os.path.join(work_directory, id + '.p7s')
   filename_object = os.path.join(work_directory, id + '.file')
   filename_pkcs7 = os.path.join(work_directory, id + '.pkcs7')
   filename_certificate = os.path.join(work_directory, id + '.cert')
   filename_openssl_report = os.path.join(work_directory, id + '.report')

   print("Open file to write: " + filename_signature, file=sys.stderr)
   with open(filename_signature, 'wb') as f:
      f.write(signature)

   openssl_build_report(filename_signature, filename_openssl_report)

   print("Ready to convert pk7 to pkcs7", file=sys.stderr)      
   convert_pk7_to_pkcs7(filename_signature, filename_pkcs7)
   
   print("Extract certificate from pkcs7", file=sys.stderr)      
   extract_certificate_from_pkcs7(filename_pkcs7, filename_certificate)

   result = parse_signature(filename_openssl_report, filename_certificate)

   if (is_object_url_valid(object_url) and load_from_object_url(object_url, filename_object)):
      print("Verify signature from pkcs7", file=sys.stderr)      
      result['verify'] = verify_signature(filename_pkcs7, filename_object, filename_certificate)
      print("Verify result = " + result['verify'], file=sys.stderr)      
       
   # cleanup
   try:
      cleanup(filename_signature)
      cleanup(filename_pkcs7)
      cleanup(filename_certificate)
      cleanup(filename_openssl_report)
      cleanup(filename_object)
   except:    
      print("Can't cleanup correctly for " + filename_signature, file=sys.stderr)

   return result

def openssl_build_report(filename_signature, filename_report):
    cmd = 'openssl pkcs7 -in ' + filename_signature + ' -inform DER -noout -print >' + filename_report
    os.system(cmd)

def extract_issuer_subject_from_certificate(filename_certificate):
    cmd = 'openssl x509  -inform PEM -in ' + filename_certificate + ' -noout -nameopt lname,sep_multiline,utf8 -subject'

    output = check_output(cmd, stderr=STDOUT, shell=True)
    output_decoded = output.decode('unicode-escape').encode('latin1').decode('utf-8')

    return dict(x.strip().split('=',1) for x in output_decoded.splitlines())

def extract_sign_date_from_report(filename_report):
    cmd = "cat " + filename_report + "| sed -n -e '/issuer_and_serial:/,/UTCTIME:/p' | tail -n 1 | sed -E 's/^\s+UTCTIME:\s*//'"
    output = check_output(cmd, stderr=STDOUT, shell=True)
    output_decoded = output.decode('unicode-escape').encode('latin1').decode('utf-8')
    date_raw = output_decoded.strip()
    result = ''
    try:
       # Mar 15 18:59:12 2021 GMT
       date = dt.datetime.strptime(date_raw, '%b %d %H:%M:%S %Y %Z')
       tz = pytz.timezone('Europe/Moscow')
       tz_utc = pytz.timezone('UTC')
       result = tz_utc.localize(date).astimezone(tz).strftime('%Y-%m-%d %H:%M:%S %z')
    except:
       pass  
    return result

def extract_certificate_number_from_report(filename_report):
    cmd = "cat " + filename_report + "| sed -n -e '/issuer_and_serial:/,/serial/p' | tail -n 1 | sed -E 's/^\s+serial:\s*//'"
    output = check_output(cmd, stderr=STDOUT, shell=True)
    output_decoded = output.decode('unicode-escape').encode('latin1').decode('utf-8')
    return output_decoded.strip()

def convert_pk7_to_pkcs7(filename_signature, filename_pkcs7):
    cmd = 'openssl pkcs7 -inform der -in ' + filename_signature + ' -out ' + filename_pkcs7
    os.system(cmd)

def extract_certificate_from_pkcs7(filename_pkcs7, filename_certificate):
    cmd = 'openssl pkcs7 -print_certs -in ' + filename_pkcs7 + ' -out ' + filename_certificate
    os.system(cmd)

def verify_signature(filename_pkcs7, filename_object, filename_certificate):
    result = 'bad'
    cmd = 'openssl smime -verify -binary -inform PEM -in ' + filename_pkcs7 + ' -content ' + filename_object + ' -certfile ' + filename_certificate + ' -nointern -noverify > /dev/null'
    try:
       output = check_output(cmd, shell=True, stderr=STDOUT)  
       output_decoded = output.decode('unicode-escape').encode('latin1').decode('utf-8')

       if (-1 != output_decoded.find('Verification successful')):
          result = 'valid' 
    except:
       pass 
    return result

def parse_signature(filename_openssl_report, filename_certificate):    
   subject_dict = extract_issuer_subject_from_certificate(filename_certificate)
   print(subject_dict, file=sys.stderr)

   sign_date = extract_sign_date_from_report(filename_openssl_report)
   certificate_number = extract_certificate_number_from_report(filename_openssl_report)
       
   result = {}
   result['verify'] = 'skip'
   result['date'] = sign_date
   result['certificate_number'] = certificate_number


   if ('surname' in subject_dict and 'givenName' in subject_dict):
      result['name'] = subject_dict['surname'] + ' ' + subject_dict['givenName']
   elif ('commonName' in subject_dict):
      result['name'] = subject_dict['commonName']
   else:  
      result['name'] = ''

   if ('organizationalUnit' in subject_dict and len(subject_dict['organizationalUnit'])>3):
      result['post'] = subject_dict['organizationalUnit']
   elif ('organizationalUnitName' in subject_dict and len(subject_dict['organizationalUnitName'])>3):
      result['post'] = subject_dict['organizationalUnitName']
   elif ('title' in subject_dict and len(subject_dict['title'])>3):
      result['post'] = subject_dict['title']
   else:
      result['post'] = ''

   return result

def cleanup(filename):
    try:
        if (os.path.isfile(filename)):
           os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred

def is_object_url_valid(url):
   return (url and (url[:7] == 'http://' or url[:8] == 'https://'))

def load_from_object_url(object_url, filename_object):
     print("Load object from object_url: " + object_url, file=sys.stderr)
     urllib3.disable_warnings()

     r = requests.get(object_url, allow_redirects=True, verify=False)
     open(filename_object, 'wb').write(r.content)

     print("Write object from object_url: " + object_url, file=sys.stderr)
     return os.path.exists(filename_object)



def isBase64(sb):
    try:
        if isinstance(sb, str):
        # If there's any unicode here, an exception will be thrown and the function will return false
           sb_bytes = bytes(sb, 'ascii')
        elif isinstance(sb, bytes):
           sb_bytes = sb
        else:
           raise ValueError("Argument must be string or bytes")
        return base64.b64encode(base64.b64decode(sb_bytes)) == sb_bytes

    except Exception:
        return False;

