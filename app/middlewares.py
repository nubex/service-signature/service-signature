from aiohttp import web
import json

def handleAnyException(request):
    data = {}
    data['verify'] = 'bad'
    data['date'] = ''
    data['certificate_number'] = ''
    data['name'] = ''
    data['post'] = ''
    print("Exception: {0}".format(err), file=sys.stderr)

    json_content = json.dumps(data, indent=2, ensure_ascii=False)
    print("Json response: " + json_content, file=sys.stderr)

    return web.Response(text = json_content, status=200, content_type='application/json')
    
def create_error_middleware(handleException):
   @web.middleware
   async def error_middleware(request, handler):
      try:
        return await handler(request)
      except Exception:
        return handleException
   return error_middleware


def setup_middlewares(app):
    error_middleware = create_error_middleware(handleAnyException)
    app.middlewares.append(error_middleware)

