#!/bin/bash

BASE_DIR=$(dirname $(readlink -e $0))
API_CHECK_URL="http://127.0.0.1:6789/api/check"

cat $BASE_DIR/data.json | curl --header "Content-Type: application/json" --request POST $API_CHECK_URL -i -T -

