#!/bin/bash

FILE=$1
BASE_DIR=$(dirname $(readlink -e $0))

if [ "$FILE" == "" ]; then
   FILE="$BASE_DIR/file.odt.p7s"
fi;

if [ ! -r $FILE -o ! -f $FILE ]; then
   echo "[E] Not found $FILE for signature."
   echo "Usage: ./check_console.sh [SIGNATURE FILE]"
   exit 1
fi;

cat $FILE | docker-compose -f $BASE_DIR/../docker-compose.yml run --rm signature python3 console.py

