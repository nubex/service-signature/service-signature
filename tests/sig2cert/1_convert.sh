#!/bin/bash

# У нас подпись в формате DER

# 1. Переводим в формат pkcs7
#openssl pkcs7 -in signature.sig -inform DER -noout -print
openssl pkcs7 -inform der -in signature.sig -out res.pkcs7

# 2. Выделяем сертификат из pkcs7
openssl pkcs7 -print_certs -in res.pkcs7 -out res.cert

# 3. Выделяем публичный ключ из сертификата
openssl x509 -inform pem -in res.cert -noout -pubkey > res.pubkey

