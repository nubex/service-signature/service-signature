#!/bin/bash

openssl smime -verify -binary -inform PEM -in res.pkcs7 -content file.pdf -certfile res.cert -nointern -noverify > /dev/null

