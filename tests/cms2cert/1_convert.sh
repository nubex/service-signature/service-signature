#!/bin/bash


cat signature.sig | openssl enc -base64 -d > res.decode_base64

openssl pkcs7 -inform der -in res.decode_base64 -out res.pkcs7

# 2. Выделяем сертификат из pkcs7
openssl pkcs7 -print_certs -in res.pkcs7 -out res.cert

# 3. Выделяем публичный ключ из сертификата
openssl x509 -inform pem -in res.cert -noout -pubkey > res.pubkey


