FROM rnix/openssl-gost:3

RUN apt-get update && apt-get install -y python3 python3-pip locales vim && rm -rf /var/lib/apt/lists/*

RUN sed -i '/ru_RU.UTF-8/s/^# //g' /etc/locale.gen && locale-gen

RUN pip3 install requests pytz aiohttp certifi contextvars

ENV LANG ru_RU.UTF-8  
ENV LANGUAGE ru_RU:ru  
ENV LC_ALL ru_RU.UTF-8 
ENV PYTHONIOENCODING utf-8
ENV PYTHONUNBUFFERED 1

RUN mkdir /app

COPY ./app /app

WORKDIR /app

RUN mkdir -p /app/data

EXPOSE 6789

CMD ["/usr/bin/python3", "/app/server.py"]

