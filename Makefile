ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(ROOT_DIR)/.mk-lib/common.mk

include .env
-include .env.local
export

DOCKER_COMPOSE_FILE := $(ROOT_DIR)/docker-compose.yml
DC_CMD := $(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE)

.PHONY: bash
bash: ## bash
	@$(DC_CMD) exec signature /bin/bash

.PHONY: pull-images up build start stop restart status ps clean logs watch-logs
pull-images: ## Pull docker images
	@$(DC_CMD) pull

build: ## Build all or c=<name> containers
	@$(DC_CMD) build $(c)

up: ## Start all or c=<name> containers in foreground
	@$(DC_CMD) up $(c)

start: ## Start all or c=<name> containers in background
	@$(DC_CMD) up -d --force-recreate $(c)

stop: ## Stop all or c=<name> containers
	@$(DC_CMD) stop $(c)

restart:
	@$(DC_CMD) stop $(c)
	@$(DC_CMD) up -d $(c)


status: ## Show status of containers
	@$(DC_CMD) ps

ps: status ## Alias of status

clean: ## Clean all data
	@$(DC_CMD) down

logs: ## Show all or c=<name> logs of containers
	@$(DC_CMD) logs --tail="100" $(c)

logs-watch: watch-logs

watch-logs: ## Watch logs
	@${DC_CMD} logs -f --tail="100"

.PHONY: signature

check: ## Check and info signature u=<url> (optional parameter) and s=<local path to file signature> (required parameter)
	@${DC_CMD} exec -T signature /usr/bin/python3 /app/console.py $(u) < $(s)


